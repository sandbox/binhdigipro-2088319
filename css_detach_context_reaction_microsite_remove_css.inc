<?php

/**
 * Expose blocks as context reactions.
 */
class css_detach_context_reaction_microsite_remove_css extends context_reaction {
  /**
   * Options form.
   */
  function options_form($context) {
    // Rebuild the block info cache if necessary.
		
		$values = $this->fetch_from_context($context);	
		$form['remove_all_css'] = array(
			'#type' => 'checkbox',
			'#title' => t("Remove All CSS"),
			//'#options' => $regions,
			'#default_value' => isset($values['remove_all_css'])?$values['remove_all_css']:FALSE,
		);

		
    return $form;
  }

  function execute(&$remove_css_ref) {	
		foreach ($this->get_contexts() as $k => $v) { 
      if (isset($v->reactions[$this->plugin]['remove_all_css'])) {
					$remove_all_css = $v->reactions[$this->plugin]['remove_all_css'];
          if ($remove_all_css) {
						$remove_css_ref = array();
						// Need sort the logic at here do it soon!
            $remove_css_ref = array(
							drupal_get_path('module','css_detach').'/css_detach.css' =>
							array(
								'type'=>'file',
								'weight'=>0,
								'group'=>0,
								'browsers'=> array('IE'=>TRUE,'!IE'=>TRUE),			
								'preprocess'=>TRUE,
								'every_page' => TRUE,
								'media'=>'all',
								'data' => drupal_get_path('module','css_detach').'/css_detach.css'
							),
							drupal_get_path('module','css_detach').'/css_detach_second.css' =>
							array(
								'type'=>'file',
								'weight'=>0,
								'group'=>0,
								'browsers'=> array('IE'=>TRUE,'!IE'=>TRUE),			
								'preprocess'=>TRUE,
								'every_page' => TRUE,
								'media'=>'all',
								'data' => drupal_get_path('module','css_detach').'/css_detach_second.css'
							)
						);
          }
      }
    }
  }
}
